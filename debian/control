Source: python-pypubsub
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pypubsub
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pypubsub.git
Homepage: https://github.com/schollii/pypubsub
Rules-Requires-Root: no

Package: python3-pubsub
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Breaks: python3-pypubsub
Provides: python3-pypubsub
Replaces: python3-pypubsub
Description: Python 3 publish-subcribe library
 Provides a publish-subscribe API to facilitate event-based or message-based
 architecture in a single-process application. It is pure Python
 and works on Python 3.3+. It is centered on the notion of a topic;
 senders publish messages of a given topic, and listeners subscribe to
 messages of a given topic, all inside the same process. The package also
 supports a variety of advanced features that facilitate debugging and
 maintaining topics and messages in larger desktop- or server-based
 applications.
